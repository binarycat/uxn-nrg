#!/bin/sh -e

cd "$(dirname $0)"
mkdir AppDir
mkdir AppDir/usr
mkdir AppDir/usr/bin
mkdir AppDir/usr/share

uxnasm main.tal AppDir/usr/share/nrg.rom 2>/dev/null
cp "$(whereis -b uxnemu | sed 's/^.*: //')" AppDir/usr/bin/uxnemu
cp icon.png AppDir

cat > AppDir/AppRun <<'EOF'
#!/bin/sh
d=$(dirname $0)
$d/usr/bin/uxnemu -s 5 $d/usr/share/nrg.rom $*
EOF

cat > AppDir/nrg.desktop <<EOF
[Desktop Entry]
Name=NRG
Exec=AppRun
Icon=icon
Type=Application
Categories=Game;
EOF

chmod +x AppDir/AppRun

appimagetool AppDir
rm -r AppDir
