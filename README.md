# NRG

This game uses the [Uxn Virtual Machine](https://wiki.xxiivv.com/site/uxn.html)
to be portable to different platforms.

To build and run the game, download and install the
[Uxn Toolchain](https://sr.ht/~rabbits/uxn/).

Once you have this installed, run the following command:

    uxnasm main.tal nrg.rom && uxnemu -s 3 nrg.rom

There is also a script to bundle an appimage, if you would prefer that.
running this requires the uxn toolchain mentioned above as well
as `appimagetool`.

## Controls
Playing with a controller is reccomended, but the keyboard mappings are
as follows:

Ctrl — A
Alt — B
Shift — SELECT
Esc or Home — START
Arrow keys — DPAD

## Difficulty

### 0 — Recon
no module costs, no passive drain, no damage
make your way through the level without worrying about failure.

### 1 — Playground
* no module costs
* no passive drain
* reduced damage
* more energy

### 2 — Casual
* no passive drain
* reduced module costs
* somewhat reduced damage
* more energy

### 3 — Lite
* more energy
* slightly reduced module costs

### 4 — Default

### 5 — Challange
* less energy
* slightly increased passive drain

### 6 — Extreme
* incresed passive drain
* less energy
* increased damage

### 7 — Unreasonable
* incresed passive drain
* less energy
* increased damage
* increased module costs
